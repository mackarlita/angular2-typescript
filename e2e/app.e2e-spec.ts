import { MyexpPage } from './app.po';

describe('myexp App', function() {
  let page: MyexpPage;

  beforeEach(() => {
    page = new MyexpPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
