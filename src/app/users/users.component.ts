import { Component } from '@angular/core';
import { UsersService } from './users.service';
import { User } from './user';

@Component({
  selector: 'users-list',
  templateUrl: 'users-list.html'
})
export class UsersListComponent {
  public users: User[];

  constructor(usersService: UsersService){
    this.users = usersService.getUsers();
  }
}
