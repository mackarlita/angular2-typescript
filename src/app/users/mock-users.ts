import { User } from './user';

export var USERS: User[] = [
  { id: 1, name: 'Karla Martin' },
  { id: 2, name: 'Marisol Colón' },
  { id: 3, name: 'Gabriel Acosta' },
  { id: 4, name: 'Luis Naranjo' }
];
